/**
 * Copyright @2019 Josin All Rights Reserved.
 * Author: Josin
 * Email : xeapplee@gmail.com
 */

#ifndef FASTCJSON_FC_TOOL_H
#define FASTCJSON_FC_TOOL_H

#include <fc_config.h>
#include <fc_string.h>
#include <fc_log.h>

#if FC_DEBUG
    #define FC_ERROR_EXIT(...) do { _logger(LOG_ERROR, __VA_ARGS__); fprintf(stderr, __VA_ARGS__); exit(0); } while(0)
#else
    #define FC_ERROR_EXIT(...) do { _logger(LOG_ERROR, __VA_ARGS__); } while(0)
#endif

#endif /* FASTCJSON_FC_TOOL_H */
