/**
 * Copyright @2019 Josin All Rights Reserved.
 * Author: Josin
 * Email : xeapplee@gmail.com
 */

#ifndef FASTCJSON_FC_CONFIG_H
#define FASTCJSON_FC_CONFIG_H

/**
 * @brief Introduction
 * Common C standard headers
 */
#include <stdio.h>
#include <stdarg.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <errno.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>

#define FC_OFFSET(type, field) ((size_t)&(((type *)0)->field))
#define container_of(p, type, mem) ( (char *)p - FC_OFFSET(type, mem) )


enum { LOG_INFO = 0, LOG_NOTICE, LOG_WARNING, LOG_ERROR };

#define elif  else if

#define TRUE     1
#define FALSE    0

/**
 * @brief NOTICE
 * SET XML_STRICT mode to TRUE, will let the kernel engine to parse the XML
 * to be strictly, for normal mode, unopend comment will be omitted, but in strict
 * mode, it will let engine to report syntax wrong.
 * default is FALSE
 */
#define XML_STRICT FALSE


#define FC_DEBUG 1

#endif /* FASTCJSON_FC_CONFIG_H */
