/**
 * Copyright @2019 Josin All Rights Reserved.
 * Author: Josin
 * Email : xeapplee@gmail.com
 */

#include <xml_demo.h>


/**
 * @brief NOTICE
 * Simple example for parsing the XML data
 */
CXML_FIELD_FUNC(root)
    CXML_FIELD_CMP(component,   component,    9,   if)
    CXML_FIELD_CMP(information,      info,   11, elif)
CXML_FIELD_FUNC_END();

CXML_FIELD_FUNC(info)
    CXML_FIELD_CMP(name,     name,   4, if)
    CXML_FIELD_INDEX_CMP(age,   1, elif)
    CXML_FIELD_INDEX_CMP(age1,  2, elif)
    CXML_FIELD_CMP(address,  addr,   7, elif)
    CXML_FIELD_CMP(os,         os,   2, elif)
CXML_FIELD_FUNC_END();

CXML_FIELD_FUNC(os)
    CXML_FIELD_CMP(linux,     lux,   5,   if)
    CXML_FIELD_CMP(windows,  windows, 7, elif)
    CXML_FIELD_CMP(unix,      unx,    4, elif)
CXML_FIELD_FUNC_END();

CXML_FIELD_FUNC(component)
    CXML_FIELD_INDEX_CMP(list,    0,   if)
    CXML_FIELD_INDEX_CMP(ignored, 1, elif)
    CXML_FIELD_INDEX_CMP(option0, 2, elif)
    CXML_FIELD_INDEX_CMP(option1, 3, elif)
    CXML_FIELD_INDEX_CMP(option2, 4, elif)
    CXML_FIELD_INDEX_CMP(option3, 5, elif)
    CXML_FIELD_INDEX_CMP(option4, 6, elif)
CXML_FIELD_FUNC_END();

CXML_ATTR_FUNC(list)
    CXML_ATTR_CMP(default, def,       7,   if)
    CXML_ATTR_CMP(id,       id,       2, elif)
    CXML_ATTR_CMP(name,   name,       4, elif)
    CXML_ATTR_CMP(comment,  comment,  7, elif)
CXML_ATTR_FUNC_END();

CXML_ATTR_FUNC(option)
    CXML_ATTR_CMP(name,   name,       4, elif)
    CXML_ATTR_CMP(value,  value,      5, elif)
CXML_ATTR_FUNC_END();


