/**
 * Copyright @2019 Josin All Rights Reserved.
 * Author: Josin
 * Email : xeapplee@gmail.com
 */

#ifndef FASTXML_XML_DEMO_H
#define FASTXML_XML_DEMO_H

#include <fc_xml.h>

/**
 * @brief NOTICE
 * Simple example for using the macros
 */
CXML_FIELD_DEF(root)
    *component, *info
CXML_FIELD_DEF_END(root);
CXML_FIELD_FUNC_DEF(root);

CXML_FIELD_DEF(info)
    *name, *age, *addr, *os, *age1
CXML_FIELD_DEF_END(info);
CXML_FIELD_FUNC_DEF(info);

CXML_FIELD_DEF(os)
    *lux, *windows, *unx
CXML_FIELD_DEF_END(os);
CXML_FIELD_FUNC_DEF(os);

CXML_FIELD_DEF(component)
    *list, *ignored, *option0, *option1, *option2, *option3, *option4
CXML_FIELD_DEF_END(component);
CXML_FIELD_FUNC_DEF(component);

CXML_ATTR_DEF(list)
    *def, *id, *name, *comment
CXML_ATTR_DEF_END(list);
CXML_ATTR_FUNC_DEF(list);

CXML_ATTR_DEF(option)
    *name, *value
CXML_ATTR_DEF_END(option);
CXML_ATTR_FUNC_DEF(option);


#endif /* FASTXML_XML_DEMO_H */
