/**
 * Copyright @2019 Josin All Rights Reserved.
 * Author: Josin
 * Email : xeapplee@gmail.com
 *
 * Hello, This is the fast xml's parsing example
 * To simplify the coding, the thing you want to do
 * is to add some macros, Let's have a try.
 */

#include <stdio.h>
#include <xml_demo.h>
#include <time.h>

int main(int argc, char *argv[])
{
    char *xml_str = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n"
                    "<information>\n"
                    "    <name id=\"IDNAME\" ws=\"Haha\" ><!--<![CDATA[Josin]]><![CDATA[Josin]]>-->hello</name>  bbb\n"
                    "    <age a=\"A\">26</age>bsfsf\n"
                    "sf  <age prop=\"price\">26.6</age>\n"
                    "    <address province=\"Guangdong\">Shenzhen Guangdong</address>\n"
                    "    <os attrs=\"ATTRs\">"
                    "       fsfs<linux><!--<![CDATA[aa]]>-->hello[[<![CDATA[Hello World]]></linux>"
                    "       <unix>aa</unix>fsf"
                    "       <windows>bb</windows>"
                    "    </os>"
                    "</information>";
    CXML *xml = new_cxml_from_string2(xml_str, strlen(xml_str));

    if ( !xml ) {
        printf("{%s}\n", new_cxml_get_error());
        trash_cxml(xml);
        return 0;
    }
    
    CXML_root *root = NEW_CXML_root_FROM_DATA(xml->data);
    printf("根节点信息\n---------------\n");
    printf("节点名:  %s\n", root->info->key->s);
    printf("节点文本:%s\n", root->info->sval->s);

    printf("\ninfo节点信息\n---------------\n");
    CXML_info *info = NEW_CXML_info_FROM_DATA(root->info->val);
    printf("%s: %s\n", info->name->key->s, info->name->sval->s);
    printf("%s: %s\n", info->age->key->s, info->age->sval->s);
    printf("%s: %s\n", info->age1->key->s, info->age1->sval->s);
    printf("%s: %s\n", info->addr->key->s, info->addr->sval->s);

    printf("\nos节点信息\n---------------\n");
    CXML_os *os = NEW_CXML_os_FROM_DATA(info->os->val);
    printf("%s: %s\n", os->lux->key->s, os->lux->sval->s);
    printf("%s: %s\n", os->unx->key->s, os->unx->sval->s);
    printf("%s: %s\n", os->windows->key->s, os->windows->sval->s);
    
    printf("\n通过循环获取同一级的所有节点信息:\n--------------------------\n");
    CXML_NODE *cnode;
    CXML_ATTR  *attr;
    CXML_LOOP_FREACH_DATA(root->info->val, cnode) {
        
        printf("%s:%s\n", cnode->key->s, cnode->sval->s);
        
        CXML_LOOP_FREACH_ATTR(cnode->attrs, attr) {
            
            printf("\t%s:%s\n", attr->key, attr->val);
            
        } CXML_LOOP_FOREACH_END();
        
    } CXML_LOOP_FOREACH_END();

    TRASH_CXML_os(os);
    TRASH_CXML_root(root);
    TRASH_CXML_info(info);
    
    trash_cxml(xml);
    
    return 0;
}